#ifndef _APP_BUTTON_H
#define _APP_BUTTON_H

#include "type_define.h"
typedef struct{
    uint8_t Blood_Flag:1;
    uint8_t Asi_Flag:1;
    uint8_t SetM_Flag:1;
    uint8_t Power_Flag:1;
}Double_Check_t;

void RegButton(void);
void TaskButton(void);
void App_Button_Init(void);
void Init_Double_Check(void);

#endif