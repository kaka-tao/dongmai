#include  "sys_config.h"
#include "multibutton.h"
#include "bsp_key.h"
#include "App_Button.h"
#include "main.h"

static button  button_MeaBlood;
static button  button_MeaAsi;
static button  button_SetM;
static button  button_Power;

/**********************************************************
* @brief 按键初始化 
* @param 
* @return 
**********************************************************/
void App_Button_Init(void){
	RegButton();
}

/**********************************************************
* @brief 注册按键 
* @param 
* @return 
**********************************************************/
void RegButton(void)
{
	ButtonInit(&button_MeaBlood, GetKey_MeaBlood_State, BUTTON_DOWN,50);
	ButtonStart(&button_MeaBlood);
	
	ButtonInit(&button_MeaAsi, GetKey_MeaAsi_State, BUTTON_DOWN,50);
	ButtonStart(&button_MeaAsi);
	
	ButtonInit(&button_SetM, GetKey_SetM_State, BUTTON_DOWN,50);
	ButtonStart(&button_SetM);
	
	ButtonInit(&button_Power, GetKey_Power_State, BUTTON_DOWN,50);
	ButtonStart(&button_Power);
}


void GetEvent_Blood_Up(void){


}

void GetEvent_Asi_Up(void){


}

void GetEvent_SetM_Up(void){


}

void GetEvent_Power_Up(void){
	

}

/**********************************************************
* @brief 按键扫描任务 
* @param 
* @return 
**********************************************************/
void TaskButton(void)
{
	ButtonScan();
	switch( button_MeaBlood.event){
		case PRESS_DOWN:
			break;

		case PRESS_UP:
			GetEvent_Blood_Up();
			break;

		case LONG_PRESS_START:
			break;

		case PRESS_AFTER_LONG:
			break;

		default:
			break;
	}

	switch( button_MeaAsi.event){
		case PRESS_DOWN:
			break;

		case PRESS_UP:
			GetEvent_Asi_Up();
			break;

		case LONG_PRESS_START:
			break;

		case PRESS_AFTER_LONG:
			break;

		default:
			break;
	}

	switch( button_SetM.event){
		case PRESS_DOWN:
			break;

		case PRESS_UP:
			GetEvent_SetM_Up();
			break;

		case LONG_PRESS_START:
			break;

		case PRESS_AFTER_LONG:
			break;

		default:
			break;
	}

	switch( button_Power.event){
		case PRESS_DOWN:
			break;

		case PRESS_UP:
			GetEvent_Power_Up();
			break;

		case LONG_PRESS_START:
			break;

		case PRESS_AFTER_LONG:
			break;

		default:
			break;
	}

}
