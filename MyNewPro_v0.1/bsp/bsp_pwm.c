#include <SZC909.H>
#include "bsp_pwm.h"

#define PUMP_GPIO    P07
#define EVM_GPIO     P06
static uint8_t EMV_Value=0;

void SYS_TC0_Interrupt(void) interrupt ISRTC0
{
    TCF0=0;
}

static void PwmInit(void)
{
    //=== Init Timer/Counter 0 ===	Pump
    TC0M = 0xC6;	//Fhosc/4	 + P06 outputs PWM signal + Enable TC0 timer
    TC0CH = 0xFF;	//Init load high byte data 
    TC0RH = 0xFF;	//Init re-load high byte data 

    TC0CL = 0;	  //Init load low byte data 
    TC0RL = 0;	  //Init re-load low byte data 

    TC0DH = 0xFF; //Init duty cycle high byte data 
    TC0DL =0;		  //Init duty cycle low byte data	
}

void PumpSwitch(PumpState_t state)
{
	PUMP_GPIO = state;	        
}

void EmvSetLevel(uint8_t PWM_Data)
{
	TC0DH = 0xFF;
	TC0DL = PWM_Data;			//Init duty cycle low byte data
	EMV_Value = PWM_Data;
}

void EmvClose(void)
{
	TC0DH = 0xFF;
	TC0DL = 0xFF;			//Init duty cycle low byte data
	EMV_Value = 0xFF;
}

uint8_t EmvGetValue(void)
{
	return EMV_Value;
}
/*******************************************
* 系统进入sleep状态前PumpAndEvm的配置   
*************************************************/
void InitPumpAndEvmGoToSleep(void)
{

}
void DrvPumpAndEvmInit(void)
{
	//1输出   0输入
	P0 &= ~0xC0;       
	P0M |= 0xC0; 
	P0UR &= ~0xC0; 
}