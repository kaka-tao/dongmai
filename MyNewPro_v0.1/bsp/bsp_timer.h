#ifndef _BSP_TIMER_H
#define _BSP_TIMER_H

#include "type_define.h"

void OpenSysTimer(void);
void CloseSysTimer(void);
void Reg1msInt(void(* pFunc)(void));
void RegRtcInt(void(* pFunc)(void));
void RegVoiceInt(void(* pFunc)(void));


#endif