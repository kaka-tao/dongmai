#include <SZC909.H>
#include "bsp_timer.h"
#include "bsp_voice.h"

static DV_Voice_Struct Voice_Struct;

/************************************************
*  定义函数指针
*************************************************/
static void(* pTc0_RtcIntCbFunc)(void); 
static void(* pTc1_1msIntCbFunc)(void); 
static void(* pTc2_VoiceIntCbFunc)(void); 
/**************************************************
			T0初始化  500ms
**************************************************/
static void RtcTimerInit(void)
{
	TF0 = 0;
	T0C = 0;					//
	T0M = 0x81;				//Ext xtal 32768 Hz
	ET0	= 1;					//ET0	= Enable 
}

/**************************************************
TC1初始化  1ms
**************************************************/
static void Tc1Init_1ms(void)
{	
//	TC1CH = 252; 	
//	TC1CL = 30;      
//	TC1RH = 252;
//	TC1RL = 30; 
	TC1CH = 0xFF; 	
	TC1CL = 0x06;      
	TC1RH = 0xFF;
	TC1RL = 0x06; 
	
	TC1M &= ~0x70;   //TC1 timer clock = Fpcu/128
	TC1M &= ~0x0c;   //TC1 clock source = Fcpu
	
	TC1M |= 0x80; 				// TC1 ENABLE, 
	IEN2 |= 0x04;				  //TC1	interrupt Enable;
}
/**************************************************
TC2初始化  语音
**************************************************/
static void Tc2Init_Voice(void)
{
	TC2CH = 0xFC;
	TC2CL = 0x18;
	TC2RH = 0xFC;
	TC2RL = 0x18;
	TC2M |= 0xF0;
	I2CCON &= 0x00; // Clear I2C Control
	I2CCON |= 0x83; // TC2 Overflow
	I2CCON |= 0x40; // Enable I2C
	EI2C = 1; // I2C interrupt enable
}


/**************************************************
打开系统1ms定时器
**************************************************/
void OpenSysTimer(void)
{
	TC1M |= 0x80; 
	IEN2 |= 0x04;
}
/**************************************************
关闭系统1ms定时器
**************************************************/
void CloseSysTimer(void)
{
	TC1M &= ~0x80; 
	IEN2 &= ~0x04;
}

/**********************************************************
* @brief 注册定时器回调函数
* @param pFunc，函数指针变量，接收传入的回调函数地址
* @return 
**********************************************************/
void Reg1msInt(void(* pFunc)(void))
{
	Tc1Init_1ms();
	pTc1_1msIntCbFunc = pFunc;
}

/**********************************************************
* @brief 注册RTC定时器回调函数
**********************************************************/
void RegRtcInt(void(* pFunc)(void))
{
	RtcTimerInit();
	pTc0_RtcIntCbFunc = pFunc;
}

/**********************************************************
* @brief 注册语音定时器回调函数
**********************************************************/
void RegVoiceInt(void(* pFunc)(void))
{
	Tc2Init_Voice();
	pTc2_VoiceIntCbFunc = pFunc;
}


/****************************************************
TC1中断  1ms
****************************************************/
void TC1_ISR(void)	interrupt ISRTC1//1ms一次
{
	TCF1 = 0; 
    pTc1_1msIntCbFunc();
}

/*****************************************************
T0中断   500ms
*****************************************************/
void SYS_RTC_Interrupt(void)	interrupt ISRTimer0
{
	TF0 = 0;
	pTc0_RtcIntCbFunc();
	//fRTC_500ms=1;
}


/*****************************************************
TC2中断   语音
*****************************************************/
void I2C_ISR(void) interrupt ISRI2c // Vector @ 0x43
{
	 switch (I2CSTA)
	 {
		 case 0x08:
			I2CCON &= 0xDF; // START (STA) = 0
			I2CDAT = Voice_Struct.Addr; // Tx/Rx addr
			break;
		 case 0x18: // write first byte
			I2CDAT = Voice_Struct.Data[Voice_Struct.Cnt++];
			Voice_Struct.Len--;
			break;
		 case 0x28: // write n byte
			if (Voice_Struct.Len == 0) { // chk length if empty
				I2CCON |= 0x10; // STOP (STO)
				Voice_Struct.Flag = 0;
				Voice_Struct.Cnt = 0;
			}
			else {
				I2CDAT = Voice_Struct.Data[Voice_Struct.Cnt++];
				Voice_Struct.Len--;
			}
			break;
		 
		 // rx mode 
		 case 0x40: // get slave addr
			I2CCON |= 0x04; // AA = 1
			break;
		 case 0x50: // read n byte
			Voice_Struct.Data[Voice_Struct.Cnt++] = I2CDAT; 
			if (Voice_Struct.Cnt == (Voice_Struct.Len-1)) {
				I2CCON &= 0xFB; // AA = 0
			}
			else { 
				I2CCON |= 0x04; // AA = 1
			}
			break;
		 case 0x58: // read last byte & stop
			Voice_Struct.Data[Voice_Struct.Cnt] = I2CDAT; 
			I2CCON |= 0x10; // STOP (STO)
			Voice_Struct.Flag = 0;
			break;
		 default:
			I2CCON |= 0x10; // STOP (STO)
			Voice_Struct.Flag = 0;
	 }
	I2CCON &= 0xF7; // Clear I2C flag (SI) 
}






