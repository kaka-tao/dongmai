#ifndef _BSP_VOICE_H
#define _BSP_VOICE_H

#include "type_define.h"



typedef struct
{
	uint8_t Cnt;
	uint8_t Len;
	uint8_t Data[5];
	uint8_t Addr;
	uint8_t Flag;
}DV_Voice_Struct;


void VoiceInit(void);
void I2CPro(void);
void InitVoiceGoToSleep(void);



#endif





