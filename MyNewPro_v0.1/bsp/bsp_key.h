#ifndef _BSP_KEY_H
#define _BSP_KEY_H

#include "type_define.h"
#define BUTTON_UP             1
#define BUTTON_DOWN           0

void InitButtonGpio(void);
void InitButtonGoToSleep(void);

BOOL GetKey_MeaBlood_State(void);
BOOL GetKey_MeaAsi_State(void);
BOOL GetKey_SetM_State(void);
BOOL GetKey_Power_State(void);

#endif