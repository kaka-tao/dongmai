#include <SZC909.H>
#include "bsp_adc.h"

/***********************************************************
	电量通道AD初始化
***********************************************************/
void AdcBatteryInit(void)
{
	ADCM1 &= 0xFE;				//0 ADC			
	VREG = 0xD8;				//1 Band Gap & AVDDR & ACM + AVDDR = 3.3v + Charge pump clock = 500k Hz + Charge pump mode = 0 Mode
	AMPM = 0x23;				//0 GX Buf + PGIA chopper clock = 7.8kHz(/32) + PGIA Gain = x1 + 1 PGIA Chopper & PGIA 	
	ADCM1= 0x76;				//0 GR Buf + Vref = 0.99v + ADC Gain x1 + OP1/OP2 Chopper 1 + "0 ADC	"(last 1)
	ADCM2= 0x14;				//ADC Clock = 250kHz + OSR = 1953(2k)Hz(250k/128) + DRDY flag clear
	CHS	 = 0x2C;				//AIN3 + AVSS	
	ADCM1 |= 0x01;			    //1 ADC	
	ADCM2 &= 0xEF;
}
/***********************************************************
	压力通道AD初始化
***********************************************************/
void AdcBpInit(void)
{
	ADCM1 &= 0xFE;			//0 ADC	
	VREG = 0xD8;				//1 Band Gap & AVDDR & ACM + AVDDR = 3.3v + Charge pump clock = 500k Hz + Charge pump mode = 0 Mode
	AMPM = 0x2F;				//0 GX Buf + PGIA chopper clock = 7.8kHz(/32) + PGIA Gain = x16 + 1 PGIA Chopper & PGIA 
	ADCM1= 0x56;				//0 GR Buf + Vref = 0.99v + ADC Gain x1 + OP1/OP2 Chopper 1 + "0 ADC	"(last 1)
	//ADCM2= 0x14;				//ADC Clock = 250kHz + OSR = 61Hz(250k/256) + DRDY flag clear
	ADCM2= 0x12;				//ADC Clock = 250kHz + OSR = 61Hz(250k/128) + DRDY flag clear
	CHS	 = 0x10;				//AN2 + AN1	//Re-check PGIA +-
	OPM = 0x01;
	ADCM1 |= 0x01;			    //1 ADC	
}

/*******************************************
* 系统进入sleep状态前adc的配置     
*************************************************/
void InitAdcGoToSleep(void)
{
   
}

/***********************************************************
	获取电压
***********************************************************/
uint8_t GetBatValue(void)
{
  while(!(ADCM2 & 0x01));
  ADCM2 &= ~0x01;
   
	return ADCDH;
}
/***********************************************************
	获取传感器adc值
***********************************************************/
uint32_t GetBpSensorValue(void)
{
	uint32_t  AdcValue = 0;
	
  while(!(ADCM2 & 0x01));
  ADCM2 &= ~0x01;
	
	AdcValue = ADCDH + 0x80;				//adc data add offset for transfer all data 0~16777216(24bit case)
	AdcValue = AdcValue & 0x00FF; //2018.01.19 10:09  防止VOUT->VOUT+时 加
	AdcValue = (AdcValue << 8) + ADCDM;
	AdcValue = (AdcValue << 8) + ADCDL;	
   
	return AdcValue;
}