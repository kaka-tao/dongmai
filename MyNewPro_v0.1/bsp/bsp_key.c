#include <SZC909.H>
#include "bsp_key.h"


#define KEY_MEA_ASI            (P11)
#define KEY_MEA_BLOOD          (P12)
#define KEY_SET_M              (P13)
#define KEY_POWER              (P14)

/*******************************************
* 初始化按键     
*************************************************/
void InitButtonGpio(void)
{
	 P1UR |= 0x1E;//打开上拉
	 P1M &= ~0x1E; //input mode
	 P1W |= 0x1E;//打开wake功能
}
/*******************************************
* 系统进入sleep状态前按键的配置     
*************************************************/
void InitButtonGoToSleep(void)
{
   
}
/*******************************************
* 获取按键状态
*************************************************/
BOOL GetKey_MeaBlood_State(void)
{
	return (BOOL)KEY_MEA_BLOOD;
}
BOOL GetKey_MeaAsi_State(void)
{
	return (BOOL)KEY_MEA_ASI;
}
uint8_t GetKey_SetM_State(void)
{
  return (BOOL)KEY_SET_M;
}
BOOL GetKey_Power_State(void)
{
  return (BOOL)KEY_POWER;
}