#ifndef _BSP_PWM_H
#define _BSP_PWM_H

#include "type_define.h"

typedef enum
{
	PUMP_OFF=0,
	PUMP_ON,
}PumpState_t;

void DrvPumpAndEvmInit(void);
void PumpSwitch(PumpState_t state);
void EmvSetLevel(uint8_t PWM_Data);
void EmvClose(void);
uint8_t EmvGetValue(void);

#endif