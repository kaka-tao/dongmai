#include <SZC909.H>
#include "bsp_flash.h"


uint8_t Save_Data;



void ISP_WriteByte(uint8_t write_data,uint16_t write_addr)
{
	EAL = 0;
	Save_Data = write_data;
	PERAM = Save_Data;
	PEROMH = write_addr>>8;
	PEBYTE = write_addr&0x3f;
	PEROML = (write_addr&0xC0) | 0X0A;
	PECMD=0x1E;			//Program command  
	_nop_();_nop_();
	EAL = 1;
}



uint8_t ISP_ReadByte(uint16_t read_addr)
{
	uint8_t code *read=(uint8_t *)read_addr;
	
	return *read;
}




