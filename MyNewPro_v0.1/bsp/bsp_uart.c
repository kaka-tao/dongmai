#include <SZC909.H>
#include "bsp_uart.h"


static uint8_t xdata u8TxFlag = 0;
static uint8_t xdata recInd=0;
static uint8_t xdata BP_Asi=0;
static BOOL u8RecFlag=0;


static void UartIoInit(void)
{
	P04 = 1;
    P05 = 0;	
    P0M = (P0M | 0x10) & ~0x20;	// set bit4 and clear bit5
}


void UartInit(void)
{
	UartIoInit();
	
	S0CON = 0x50;
    SM20 	= 0;						    // Disable multiprocessor communication
    S0CON2= 0x80;					    // Baud mode form internal baud rate
    PCON	=	0x80;					// baudrate*2
    S0RELH = 0x03;							// Set baud rate
    S0RELL = 0xF7;							// baud rate = 9600(Fcpu = 32M Hz)		
    ES0	=	1;						    // Enable UART interrupt	
}


void Uart_Interrupt(void)	interrupt ISRUart
{
	if (TI0){ //检查发送中断标志位
		TI0 = 0;
		u8TxFlag = 1;
		WDTR = 0x5A;
	}
   if (RI0){ //检查接收标志位
		RI0 = 0;
		if ((recInd==0)&&(S0BUF==0xFA)) 		recInd++;//初始头
		else if ((recInd==1)&&(S0BUF==0xAF)) 	recInd++;
		else if (recInd==2){
			if (S0BUF==0x01) {
				BP_Asi=0;
				recInd=0;
				u8RecFlag=1;
			}
			else if (S0BUF==0x02) recInd++;
		}
		else if (recInd==3){
			BP_Asi=S0BUF;
			recInd=0;
			u8RecFlag=1;
		}
		else									recInd=0;
    }
}



void UART_Tx_Byte(uint8_t Tx_Data)
{
    S0BUF = Tx_Data;
    while(!u8TxFlag);                       // wait for end of transmit
    if (u8TxFlag) 
    {
        u8TxFlag = 0;
    }
}


void UART_Tx_NORMAL(uint8_t *s,uint8_t l)
{
    int i;
    for(i=0;i<l;i++)
	{
		UART_Tx_Byte(*s++);
    }
}


void UART_Tx_String(uint8_t *s)
{
    while(*s)
    {
        UART_Tx_Byte(*s++);
    }
}






