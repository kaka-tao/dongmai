#ifndef __MULTIBUTTON_H__
#define __MULTIBUTTON_H__

typedef void (*BtnCallback)(void*);

//按键事件
typedef enum {
	NONE_ButEvent = 0,
    PRESS_DOWN,      //按键按下，每次按下都触发
    PRESS_UP,        //按键弹起，每次松开都触发
    LONG_PRESS_START,//达到长按时间阈值时触发一次
    LONG_PRESS_HOLD, //长按期间一直触发
	PRESS_AFTER_LONG,//长按后的弹起
    NUMBER_event, 
}PressEvent;

//按键状态
typedef enum{
	STATE_UP = 0,
	STATE_DOWN,
	STATE_LONG_DOWN,
}PressState;

typedef struct button
{
	uint8_t      pressFlag        :1;
	uint8_t      longPressFlag    :1;
	uint8_t      startPressFlag   :1;

	PressState   state;

	uint16_t     long_ticks;
	
	uint8_t      press_cnts;
	uint8_t      (*Hal_button_level)(void);
	uint8_t      active_gpio_state;
	PressEvent   event;
	BtnCallback  cb[NUMBER_event];
	
	struct button* next;
}button;



void ButtonInit(button *handle, uint8_t(*pin_level)(void), uint8_t active_level,uint16_t long_ticks);
int  ButtonStart(struct button* handle);
void ButtonStop(struct button* handle);
PressEvent GetButtonEvent(struct button* handle);
void ButtonScan(void);














#endif