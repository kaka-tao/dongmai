#include  "sys_config.h"

#if defined  (USE_SOFT_TIMER)
#include "soft_timer.h"

//timer handle list head.
static struct Timer* timer_head_handle = NULL;
/**
  * @brief  Initializes the timer struct handle.
  * @param  handle: the timer handle strcut.
  * @param  timeout_cb: timeout callback.
  * @param  repeat: repeat interval time.
  * @retval None
  */
void timer_init(struct Timer* handle, void(*timeout_cb)(), uint32_t cycle, TIMER_KIND_enum kind)
{
  handle->kind = kind;
	handle->timeout_cb = timeout_cb;
	handle->timeout = cycle/5;
	handle->cnts = 0;
}

/**
  * @brief  Start the timer work, add the handle into work list.
  * @param  btn: target handle strcut.
  * @retval 0: succeed. -1: already exist.
  */
int timer_start(struct Timer* handle)//遍历单链表，采用在链表头部插节点的方法
{
	struct Timer* target = timer_head_handle;
	while(target) {
		if(target == handle) return -1;	//already exist.
		target = target->next;
	}
	handle->next = timer_head_handle;
	timer_head_handle = handle;
	return 0;
}

/**
  * @brief  Stop the timer work, remove the handle off work list.
  * @param  handle: target handle strcut.
  * @retval None
  */
void timer_stop(struct Timer* handle)
{
	struct Timer** curr;
	for(curr = &timer_head_handle; *curr; ) {
		struct Timer* entry = *curr;
		if (entry == handle) {
			*curr = entry->next;
//			free(entry);
		} else
			curr = &entry->next;
	}
}

/**
  * @brief  main loop.
  * @param  None.
  * @retval None
  */
void timer_loop()
{
	struct Timer* target;
	for(target=timer_head_handle; target; target=target->next) {
		if(++target->cnts >= target->timeout) {
			if(target->kind == TIMER_KIND_ONESHOT) {
				timer_stop(target);
			}
            target->cnts=0;
			target->timeout_cb();
		}
	}
}

#endif


