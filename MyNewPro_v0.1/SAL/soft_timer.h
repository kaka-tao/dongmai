#ifndef __SOFT_TIMER_H__
#define __SOFT_TIMER_H__



typedef enum _timer_kind_
{
	TIMER_KIND_ONESHOT,							/* Oneshot timer		*/
	TIMER_KIND_CYCLE,							/* Cycle timer			*/
	TIMER_KIND_MAX								/* MAX 					*/
}TIMER_KIND_enum;

typedef struct Timer {
    TIMER_KIND_enum  kind;
    uint32_t timeout;
    uint32_t cnts;
    void (*timeout_cb)(void);
    struct Timer* next;
}Timer;


void timer_init(struct Timer* handle, void(*timeout_cb)(), uint32_t cycle, TIMER_KIND_enum kind);
int  timer_start(struct Timer* handle);
void timer_stop(struct Timer* handle);
void timer_ticks(void);
void timer_loop(void);









void TIMER0_INITIAL(void);






















#endif