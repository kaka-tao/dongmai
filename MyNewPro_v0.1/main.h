#ifndef __MAIN_H__
#define __MAIN_H__

#include "type_define.h"

//系统主状态
typedef enum{
    M_State_Poweroff = 0 , //关机
    M_State_Standby,       //待机
    M_State_Memory,        //记忆
    M_State_Measure,       //测量
    M_State_Error,         //报错
    M_State_Debug,         //Debug
    M_State_Setting,       //设置
}Main_State_t;

//系统总参数
typedef struct{
    Main_State_t     mainState; //主状态 
}SysInfo_t;

extern SysInfo_t m_sysInfo;
#endif