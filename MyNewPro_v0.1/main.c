#include  "sys_config.h"
#include  "App_Button.h"
#include  "App_timer.h"
#include  "App_Led.h"
#include  "App_Voice.h"
#include  "sysclk.h"
#include  "bsp_pwm.h"
#include  "bsp_key.h"
#include  "bsp_voice.h"
#include  "bsp_uart.h"
#include  "main.h"

volatile uint32_t g_sysRunTime = 0;/**sleep前清0*******/
volatile SysInfo_t m_sysInfo;

typedef struct
{
  uint8_t   Run;             //任务状态：Run/Stop
  uint16_t  TimCount;        //时间片周期，用于递减函数
  uint16_t  TRITime;         //时间片周期，用于函数重载
  void (*TaskHook)(void);    //保存任务函数地址	
}TASK_COMPONENTS;

//在这里注册任务
static TASK_COMPONENTS g_Task_Comps[] = 
{
        //状态    计数    周期    函数
		//{ 0,     1,     1,     TaskCatch},    /****5ms****/
		{ 0,      3,      3,     TaskDisplay},
		{ 0,     50,     50,     TaskButton},   /****50ms****/
		{ 0,     100,    100,    TaskVoice},    /****100ms****/
		{ 0,     100,    100,    TaskDisplay},  /****100ms****/
};

#define   TasksMax       (sizeof(g_Task_Comps)/sizeof(g_Task_Comps[0])) 

/**********************************************************
* @brief 系统参数初始化
* @param 
* @return 
**********************************************************/
static void SysInfo_Init(void){
	memset( &m_sysInfo , 0 , sizeof( m_sysInfo ) );
}

/**********************************************************
* @brief 系统任务扫描入口
* @param 
* @return 
**********************************************************/
static void TaskHandler(void)
{
	 volatile uint8_t i;
	 
	 for(i = 0;i < TasksMax;i++)
	 {
		 if(g_Task_Comps[i].Run)
		 {
				g_Task_Comps[i].Run = 0;//标记清零
				g_Task_Comps[i].TaskHook();//执行任务
		 }
	 }
} 

/**********************************************************
* @brief 1ms定时
* @param 
* @return 
**********************************************************/
static void TimerTaskScan(void)
{
	uint8_t i;
	
	g_sysRunTime++;/******sleep前需要清0*********/
	
	for(i = 0;i < TasksMax;i++)
	{
		if(--g_Task_Comps[i].TimCount == 0)
			{
				 //时间标记为1，并重载计数初值
				 g_Task_Comps[i].TimCount = g_Task_Comps[i].TRITime;
				 g_Task_Comps[i].Run = 1;	
			}
	}
}
/**
***********************************************************
* @brief 获取系统运行时间
* @param
* @return 以1ms为单位
***********************************************************
*/
uint32_t GetSysRunTime(void)
{
	return g_sysRunTime;
}
/**
***********************************************************
* @brief 初始化驱动
* @param
* @return 
***********************************************************
*/
static void DrvInit(void)
{
	SystemClockInit();
	InitButtonGpio();
	DrvPumpAndEvmInit();
	DisplayIoInit();
	VoiceInit();
	UartInit();
	
	
	Reg1msInt(TimerTaskScan);//注册系统定时器
	RegRtcInt(RtcPro);//注册RTC事件
	RegButton();//注册按键
	
	
}
/**
***********************************************************
* @brief 初始化应用
* @param
* @return 
***********************************************************
*/
static void AppInit(void)
{
	SysInfo_Init();      //系统参数初始化
	App_Button_Init();   //按键初始化

}

void main(void)
{
	DisableSysIrq(); //Interrupt disable
	
	DrvInit();
	AppInit();
	
	EnableSysIrq();
	
	while(1)
	{
		TaskHandler();
		RtcPro();
	}
}



